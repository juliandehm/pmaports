# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=oxygen
pkgver=5.20.90
pkgrel=0
pkgdesc="Artwork, styles and assets for the Oxygen visual style for the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x and mips64 blocked by frameworkintegration
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	frameworkintegration-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	xcb-util-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/oxygen-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-sounds::noarch"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sounds() {
	pkgdesc="$pkgdesc (sounds)"
	amove usr/share/sounds
}
sha512sums="53f0ca2908939cf2fe6fca452f29b6f6f1ab4c876fc31ffcce7bf3298c0a04ee7c5e7ed40da5b8b8415535ba01da02d22b574146b51c102e904fba6aae8e7d13  oxygen-5.20.90.tar.xz"
