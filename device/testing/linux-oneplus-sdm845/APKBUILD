# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="oneplus-sdm845"
pkgname=linux-$_flavor
pkgver=5.10
pkgrel=1
pkgdesc="Kernel fork for OnePlus SDM845 devices, close to mainline"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/sdm845-linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="bison findutils flex installkernel openssl-dev perl"

_config="config-$_flavor.$arch"
_commit="ccb798850a85a6a9c8c70e02f0bf77af06c90d60"

# Source
source="
	sdm845-linux-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/sdm845-linux/-/archive/$_commit/sdm845-linux-$_commit-sdm845.tar.gz
	$_config
"
builddir="$srcdir/sdm845-linux-$_commit"

prepare() {
	default_prepare
	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$builddir" O="$srcdir"/build ARCH="$_carch" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	cd "$srcdir/build/arch/$_carch/boot"

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="93c0aa227f441a234e910ed3052fa83fd9d4d77eb044b72769129f0ca82d6a140426029b4541783e1b8c4abe5bd8b476cd53119528a5f18448fe2889bce40ed0  sdm845-linux-ccb798850a85a6a9c8c70e02f0bf77af06c90d60.tar.gz
1e31db87d3b5b41a7e61e30f40e45f40786d171f3d0e0e88fcb67c9a7f9ed7e35d8139ffa959bbef1fd258f910ba9865c8f66da985f34c88bfd9ca4d774b4a6d  config-oneplus-sdm845.aarch64"
